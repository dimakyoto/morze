def code_morze(value):
    morse_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 
        'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 
        'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 
        'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 
        'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---', '3': '...--', 
        '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', 
        '9': '----.', '0': '-----', ',': '--..--', '.': '.-.-.-', '?': '..--..', 
        '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }
    
    start_text = value.upper().replace(" ", "")

    morse_text = []
    for char in start_text:
        if char in morse_dict:
            morse_text.append(morse_dict[char])

    return ' '.join(morse_text)
